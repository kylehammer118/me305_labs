# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 15:44:05 2021

@author: Kyle Hammer
@file Fib.py
@date 9/30/21
"""

def fib (idx):
    '''
    @brief    This function calculates a Fibonacci number at a specific index.
    @param    idx is an integer specifiying the index of the desired Fibonacci number
    @return   fibonacci number at the index inputted
    '''
    if idx == 0 :   #To start the recursion we have to define the first two Fibonacci values'''
        return 0
    elif idx == 1:
        return 1
    else: 
        n = 2
        f1 = 1      #First two Fibonacci values assigned to variables'''
        f2 = 0
        while n <= idx:  #While loop: adds previous two Fibonacci numbers and then updates them for the next run of the loop'''
            f = f1 + f2
            f2 = f1
            f1 = f
            n = n + 1   #n keeps increasing until it equals the index inputed and then the Fibonacci number is returned'''
        return f
            
go = 'blank'   
while go != 'q':
    if __name__ == '__main__':
        try: 
            idx = input('Please enter an index: ')  
            if idx == 'q':              #Loop will stop when q is inputted which ends the program'''
                break
            idxi = int(idx)         #idx is cast as an integer and then if positive will run the fib function above'''
            if idxi >= 0 :
                print ('Fibonacci number at '
                       'index {:} is {:}.'.format(idxi,fib(idxi)))
                print('Enter another index or q to quit')
                continue
            else:                  #if idx is negative error message displays and user is prompted for a further input'''
                print ('Index must be nonnegative!')
                continue
            
        except:
            print ('Index must be nonnegative!')
            continue

